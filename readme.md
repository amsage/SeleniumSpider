This program uses Selenium to go online and play a game of Spider Solitaire.
v1.0 accesses https://www.solitr.com/spider-solitaire to play an easy game.
Selenium is used to read the id values of the Cards on the board at the beginning of the game
    -Cards are in specific order at the beginning, which is taken advantage of.
Cards are dragged between each other and empty pile bases.

The program functions by playing a game with these cards internally, and then executes a series of moves online after its internal game has completed.
The program performs minimal testing on the state of the online cards; any error will go unnoticed by the program and may appear as strange behavior to the user.
Statically defined synchronization is used to avoid making moves during animations.

In order to make moves, the SmartEasyPlayer recursively generates a graph of possible moves that do not change the state of the gameboard by flipping over a new card.  
    -This graph is limited to 1000 entries in order to maximize execution speed.
    -Each visualized move involves copying the entire GameBoard structure, which is considered the largest cause of inefficiency in time and space.  Correcting this is the goal of v1.2.
    -Each potential move in the graph is scored according to a value called 'Entropy'.  Lower entropy implies a more favorable game state.  Human error in designing the entropy scoring algorithm is considered the largest cause of game-losing mistakes.
    -The sequence of moves that leads to least entropy is executed, and the process repeats.
    
The depricated (and most likely broken) DumbPlayer made moves based on static evaluations of the board with minimal lookahead.

Both players utilize an enumeration called State in order to determine the current 'goal'.

The main method of the class Main runs an internal test of a large number of games in order to score the SmartEasyPlayer on the number of wins.

The main method of SeleniumEasyGame executes the online play through Selenium.
    