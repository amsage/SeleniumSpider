package game;


import java.util.ArrayList;
import java.util.Collections;
import java.util.ListIterator;

import resources.Move;



public class GameBoard {
	public  ArrayList<Tableau> columns = new ArrayList<Tableau>();
	
	
	public ArrayList<Card[]> deals = new ArrayList<Card[]>();
	public int completedStacks = 0;

	public GameBoard(){
		for (int i = 0; i < 10; i++) {
			columns.add(new Tableau(this));
		}
	}
	
	
	
	public GameBoard(GameBoard copy){
		for(Card[] i:copy.deals){
			Card[] newDeal = new Card[10];
			for(int j=0; j<10; j++){
				newDeal[j]=new Card(i[j]);
			}
			this.deals.add(newDeal);
		}
		
		for(Tableau i: copy.columns){
			this.columns.add(new Tableau(i, this));
		}
		
		this.completedStacks = copy.completedStacks;
		
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof GameBoard)){
			return false;
		}
		
		GameBoard a = (GameBoard)(obj);
		
		return a.columns.equals(this.columns) && a.deals.size()==this.deals.size();
	};
	
	@Override
	public int hashCode() {
		return this.columns.hashCode()+this.deals.size();
	};
	
	
	
	public void dealEasyGame() {
		deals.clear();
		Deck.buildEasyDeck();
		
		completedStacks=0;
		for (int i = 0; i < 5; i++) {
			Card[] newDeal = new Card[10];
			for (int j = 0; j < 10; j++) {
				newDeal[j] = Deck.deck.remove(Random.getInt(Deck.deck.size()));
				newDeal[j].flipUp();
			}
			deals.add(newDeal);
		}

		for(Tableau i:columns){
			i.tableau.clear();
		}
		
		for (int i = 0; i < 54; i++) {
			columns.get(i % 10).tableau.add(Deck.deck.remove(Random.getInt(Deck.deck.size())));
		}

		for (Tableau i : columns) {
			i.tableau.get(i.tableau.size() - 1).flipUp();
		}

		this.parse();
	}

	public void printGame() {
		
		
		System.out.println(" __ __ __ __ __ __ __ __ __ __ ☆");
		for (int i = 0; i < Collections.max(columns).tableau.size() || i < completedStacks; i++) {
			System.out.print("|");
			for (int j = 0; j < 10; j++) {
				if (columns.get(j).tableau.size() > i) {
					System.out.print(columns.get(j).tableau.get(i) + "\u00A0");
				} else {
					System.out.print("  \u00A0");
				}
			}
			if(i<completedStacks){
				System.out.print("★");
			}
			System.out.println("");
		}
		System.out.println("");
	}

	public void dealNext() {
		Card[] newDeal = deals.remove(0);
		for (int i = 0; i < 10; i++) {
			columns.get(i).tableau.add(newDeal[i]);
			checkComplete(columns.get(i));
		}
	}
	
	public boolean isReadyToDeal(){
		if(deals.size()<=0){
			return false;
		}
		
		for(Tableau i: columns){
			if(i.tableau.size()==0){
				return false;
			}
		}
		
		return true;
	}

	public void moveCards(int count, Tableau origin, int destination) {
		moveCards(count, origin, columns.get(destination));
	}

	public int moveCards(int count, int origin, int destination) {
		moveCards(count, columns.get(origin), columns.get(destination));
		return count;
	}
	
	public void moveCards(int count, Tableau origin, Tableau destination){
		moveCards(count, origin, destination, true);
	}
	
	public int moveCards(Move move, boolean doFlip){
		return moveCards(move.count, columns.get(move.origin), columns.get(move.destination), doFlip);
	}

	public int moveCards(int count, Tableau origin, Tableau destination, boolean doFlip) {

		ArrayList<Card> stack = origin.tableau;
		ListIterator<Card> i = stack.listIterator(stack.size() - count);
		Card j;
		for (int k = 0; k < count; k++) {
			j = i.next();
			destination.tableau.add(j);
		}
		for (int t = 0; t < count; t++) {
			stack.remove(stack.size() - 1);
		}

		if (stack.size() > 0 && doFlip==true) {
			stack.get(stack.size() - 1).flipUp();
		}

		destination.process();
		checkComplete(destination, doFlip);
		
		return count;
	}
	
	private void checkComplete(Tableau destination){
		checkComplete(destination, true);
	}

	private void checkComplete(Tableau destination, boolean flipEm) {
		if (destination.getStackSize() == 13) {
			completedStacks++;
			for (int i = 0; i < 13; i++) {
				destination.tableau.remove(destination.tableau.size() - 1);
			}

			if (destination.getTopCard() != null && flipEm) {
				destination.getTopCard().flipUp();
			}
		}

	}

	public void parse() {
		for (Tableau i : columns) {
			i.process();
		}
	}

	public int fullMove(int origin, int destination) {
		columns.get(origin).process();
		columns.get(destination).process();
		return moveCards(columns.get(origin).validStackSize, origin, destination);
	}

	public void fullMove(Tableau origin, int destination) {
		moveCards(origin.validStackSize, origin, destination);
	}

	public void fullMove(Tableau origin, Tableau destination) {
		moveCards(origin.validStackSize, origin, destination);

	}

	public int halfMove(Tableau origin, Integer destination) {
		return halfMove(origin, columns.get(destination));

	}

	private int halfMove(Tableau origin, Tableau destination) {
		Card originCard = origin.getTopCard();
		Card targetCard = destination.getTopCard();
		int cardCount = targetCard.value - originCard.value;
		moveCards(cardCount, origin, destination);
		return cardCount;
	}

	public boolean isGameOver() {
		if (completedStacks == 8) {
			return true;
		} else {
			return false;
		}
	}



	public int cardCount() {
		int count = 0;
		for(Tableau i:columns){
			count+=i.tableau.size();
		}
		return count;
	}



	public int halfMove(int i, Integer j) {
		return halfMove(columns.get(i), j);
	}



	public int fullMove(int origin, int destination, boolean flipEm) {
		columns.get(origin).process();
		columns.get(destination).process();
		return moveCards(new Move(columns.get(origin).validStackSize, origin, destination), flipEm);
	}
}
