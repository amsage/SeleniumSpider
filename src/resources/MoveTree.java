package resources;

import game.GameBoard;
import game.Tableau;

public class MoveTree {
	public static boolean debug;
	public Move move = null;
	public double score = 0.0;
	public VirtualBoard board = null;
	public boolean revealing;
	
	public MoveTree(Move newMove, double newScore, GameBoard newBoard){
		move = newMove;
		score = newScore;
		board = new VirtualBoard(newBoard);
		board.virtualMove(move);
		board.calculateEntropy();
		
		if(debug){
			board.board.printGame();
			System.out.println(score);
		}
		
		for(Tableau i : board.board.columns){
			if(i.tableau.size()>0 && i.getTopCard().faceUp==false){
				revealing = true;
			}
		}
		
	}
}
